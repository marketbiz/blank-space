<?php
/**
 * Theme Option
 *
 * Plugin Name: MBZ Theme Options
 * Plugin URI:  http://www.marketbiz.net
 * Description: All General Configuration For This Theme
 * Version:     1.0.0
 * Author:      Marketbiz Development Team
 * Author URI:  http://www.marketbiz.net
 * License:     GPL-2.0+
 * Copyright:   2015 Marketbiz
 *
 * @author Marketbiz Development Team
 *
 */

function theme_menu() {
 
    add_theme_page(
        'Theme Options',            // The title to be displayed in the browser window for this page.
        'Theme Options',            // The text to be displayed for this menu item
        'administrator',            // Which type of users can see this menu item
        'theme_options',    		// The unique ID - that is, the slug - for this menu item
        'theme_display'     		// The name of the function to call when rendering this menu's page
    );
 
} // end theme_menu
add_action('admin_menu', 'theme_menu');
 
function theme_display() { 
    ?>
    <!-- Create a header in the default WordPress 'wrap' container -->
    <div class="wrap">
     
        <div id="icon-themes" class="icon32"></div>
        <h2>Theme Options</h2>
        <?php settings_errors(); ?>
         
        <?php
            if( isset( $_GET[ 'tab' ] ) ) {
                $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_settings';
            } else {
                $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_settings';
            } // end if
        ?>
         
        <h2 class="nav-tab-wrapper">
            <a href="?page=theme_options&tab=general_settings" class="nav-tab <?php echo $active_tab == 'general_settings' ? 'nav-tab-active' : ''; ?>">General Settings</a>
            <a href="?page=theme_options&tab=social_options" class="nav-tab <?php echo $active_tab == 'social_options' ? 'nav-tab-active' : ''; ?>">Social Options</a>
            <a href="?page=theme_options&tab=seo_options" class="nav-tab <?php echo $active_tab == 'seo_options' ? 'nav-tab-active' : ''; ?>">SEO Options</a>
        </h2>
         
        <form method="post" action="options.php">
 			
	        <?php
	        	if( $active_tab == 'general_settings' ) {
		            settings_fields( 'theme_general_settings' );
		            do_settings_sections( 'theme_general_settings' );
		        } else if( $active_tab == 'social_options' ) {
		            settings_fields( 'theme_social_options' );
		            do_settings_sections( 'theme_social_options' );
		        } else if( $active_tab == 'seo_options' ) {
		            settings_fields( 'theme_seo_options' );
		            do_settings_sections( 'theme_seo_options' );
		        } // end if/else
	        ?>         
            <?php submit_button(); ?>
             
        </form>
         
    </div><!-- /.wrap -->
<?php
     
} // end theme_display

/* ------------------------------------------------------------------------ *
 * Script Registration
 * ------------------------------------------------------------------------ */

function options_enqueue_scripts() {
    wp_register_script( 'logo-upload', get_template_directory_uri() .'/assets/vendor/logo-upload/logo-upload.js', array('jquery','media-upload','thickbox') );

    if ( 'appearance_page_theme_options' == get_current_screen() -> id ) {
        wp_enqueue_script('jquery');
 
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
 
 		wp_enqueue_media();
        wp_enqueue_script('media-upload');
        wp_enqueue_script('logo-upload');
    } 
}
add_action('admin_enqueue_scripts', 'options_enqueue_scripts');
 
/* ------------------------------------------------------------------------ *
 * Setting Registration
 * ------------------------------------------------------------------------ */

function general_settings_theme_options() {
 
     // If the theme options don't exist, create them.
    if( false == get_option( 'theme_general_settings' ) ) {  
        add_option( 'theme_general_settings' );
    } // end if
 
    // First, we register a section. This is necessary since all future options must belong to a 
    add_settings_section(
        'general_settings_section',         // ID used to identify this section and with which to register options
        'General Settings',                 // Title to be displayed on the administration page
        'general_settings_callback', 		// Callback used to render the description of the section
        'theme_general_settings'     		// Page on which to add this section of options
    );
     
    // Next, we'll introduce the fields for toggling the visibility of content elements.

    add_settings_field(
    	'weblogo',  
    	'Website Logo', 
    	'weblogo_callback', 
    	'theme_general_settings', 
    	'general_settings_section',
    	array('Tambahkan Logo pada Website Anda.')
    );

    add_settings_field(
    	'weblogo_preview',  
    	'Logo Preview', 
    	'weblogo_preview_callback', 
    	'theme_general_settings', 
    	'general_settings_section'
    );

    add_settings_field(
    	'webfavicon',  
    	'Website Favicon', 
    	'webfavicon_callback', 
    	'theme_general_settings', 
    	'general_settings_section',
    	array('Tambahkan Favicon Logo pada Website Anda.')
    );

    add_settings_field(
    	'webfavicon_preview',  
    	'Favicon Preview', 
    	'webfavicon_preview_callback', 
    	'theme_general_settings', 
    	'general_settings_section'
    );

    add_settings_field(
    	'google_map',
    	'Google Map',
    	'google_map_callback',
    	'theme_general_settings',
    	'general_settings_section',
    	array('Masukkan Embed Code dari Google Map Anda.')
    );

    add_settings_field(
    	'google_map_preview',
    	'Google Map Preview',
    	'google_map_preview_callback',
    	'theme_general_settings',
    	'general_settings_section'
    );

    add_settings_field(
    	'meta_slider',
    	'Meta Slider Shortcode',
    	'meta_slider_callback',
    	'theme_general_settings',
    	'general_settings_section',
    	array('Masukkan Shortcode dari <a href="'.admin_url("admin.php?page=metaslider").'">Meta Slider</a> Anda.')
    );

    add_settings_field(
    	'contact_form',
    	'Contact Form Shortcode',
    	'contact_form_callback',
    	'theme_general_settings',
    	'general_settings_section',
    	array('Masukkan Shortcode dari <a href="'.admin_url("admin.php?page=wpcf7").'">Contact Form</a> Anda.')
    );
     
    // Finally, we register the fields with WordPress
    register_setting(
        'theme_general_settings',
        'theme_general_settings'
    );
     
} // end general_settings_theme_options
add_action('admin_init', 'general_settings_theme_options');

function social_options_theme_options() {
 
     // If the theme options don't exist, create them.
    if( false == get_option( 'theme_social_options' ) ) {  
        add_option( 'theme_social_options' );
    } // end if
 
    // First, we register a section. This is necessary since all future options must belong to a 
    add_settings_section(
        'social_options_section',         // ID used to identify this section and with which to register options
        'Social Options',                 // Title to be displayed on the administration page
        'social_options_callback', 		// Callback used to render the description of the section
        'theme_social_options'     		// Page on which to add this section of options
    );
     
    // Next, we'll introduce the fields for toggling the visibility of content elements.
    add_settings_field( 
        'social_facebook',
        'Facebook',
        'social_facebook_callback',
        'theme_social_options',
        'social_options_section',
    	array('Masukkan Link Facebook Anda.')
    );

    add_settings_field( 
        'social_twitter',
        'Twitter',
        'social_twitter_callback',
        'theme_social_options',
        'social_options_section',
    	array('Masukkan Link Twitter Anda.')
    );

    add_settings_field( 
        'social_googleplus',
        'Google +',
        'social_googleplus_callback',
        'theme_social_options',
        'social_options_section',
    	array('Masukkan Link Google + Anda.')
    );
     
    // Finally, we register the fields with WordPress
    register_setting(
        'theme_social_options',
        'theme_social_options'
    );
     
} // end general_settings_theme_options
add_action('admin_init', 'social_options_theme_options');

function seo_options_theme_options() {
 
     // If the theme options don't exist, create them.
    if( false == get_option( 'theme_seo_options' ) ) {  
        add_option( 'theme_seo_options' );
    } // end if
 
    // First, we register a section. This is necessary since all future options must belong to a 
    add_settings_section(
        'seo_options_section',         // ID used to identify this section and with which to register options
        'SEO Options',                 // Title to be displayed on the administration page
        'seo_options_callback', 		// Callback used to render the description of the section
        'theme_seo_options'     		// Page on which to add this section of options
    );
     
    // Next, we'll introduce the fields for toggling the visibility of content elements.
       
    add_settings_field( 
        'seobar_text',                     
        'SEO Bar H1 Text',              
        'seobar_text_callback',  
        'theme_seo_options',                    
        'seo_options_section',         
        array('Tambahkan text untuk SEO Bar pada Website Anda.')
    );

    add_settings_field( 
        'meta_keywords',
        'Meta Keywords',
        'meta_keywords_callback',
        'theme_seo_options',
        'seo_options_section',
    	array('List Meta Keywords untuk Website ini.')
    );

    add_settings_field( 
        'meta_desc',
        'Meta Description',
        'meta_desc_callback',
        'theme_seo_options',
        'seo_options_section',
    	array('Meta Description untuk Website ini.')
    );
     
    // Finally, we register the fields with WordPress
    register_setting(
        'theme_seo_options',
        'theme_seo_options'
    );
     
} // end general_settings_theme_options
add_action('admin_init', 'seo_options_theme_options');
 
/* ------------------------------------------------------------------------ *
 * Section Callbacks
 * ------------------------------------------------------------------------ */
 
function general_settings_callback() {
    echo '<p>Anda dapat mengubah pengaturan umum pada menu ini.</p>';
} // end general_options_callback

function seo_options_callback() {
    echo '<p>Anda dapat mengubah pengaturan SEO pada menu ini.</p>';
} // end hnf_options_callback

function social_options_callback() {
    echo '<p>Anda dapat mengubah pengaturan media sosial pada menu ini.</p>';
} // end social_options_callback
 
/* ------------------------------------------------------------------------ *
 * Field Callbacks
 * ------------------------------------------------------------------------ */
 
/**************
function toggle_header_callback($args) {
     
    // First, we read the options collection
    $options = get_option('theme_general_settings');
     
    // Next, we update the name attribute to access this element's ID in the context of the display options array
    // We also access the show_header element of the options collection in the call to the checked() helper function
    $html = '<input type="checkbox" id="show_header" name="theme_general_settings[show_header]" value="1" ' . checked(1, $options['show_header'], false) . '/>'; 
     
    // Here, we'll take the first argument of the array and add it to a label next to the checkbox
    $html .= '<label for="show_header"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end toggle_header_callback 
**************/

function seobar_text_callback($args) {
     
    $options = get_option('theme_seo_options');
     
    $html = '<input type="text" id="seobar_text" name="theme_seo_options[seobar_text]" size="50" value="' . $options['seobar_text']. '" />'; 
    $html .= '<label for="seobar_text" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end seobar_text_callback

function weblogo_callback($args) {
     
    $options = get_option('theme_general_settings');
     
    $html = '<input type="text" id="logo_url" name="theme_general_settings[weblogo]" value="'.esc_url( $options['weblogo'] ).'" />'; 
    $html .= '<input id="upload_logo_button" type="button" class="button" value="Upload Logo" />';
    $html .= '<label for="logo_url" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end weblogo_callback

function weblogo_preview_callback($args) {
     
    $options = get_option('theme_general_settings');

    if ($options['weblogo']) {
	    $html = '<div id="upload_logo_preview" style="min-height: 100px;">
	        <img style="max-width:100%;" src="'.esc_url( $options['weblogo'] ).'" />
	    </div>';
    } else {
	    $html = '<div id="upload_logo_preview" style="min-height: 100px;">Belum ada logo.</div>';
    }
      
     
    echo $html;
     
} // end weblogo_callback 

function webfavicon_callback($args) {
     
    $options = get_option('theme_general_settings');
     
    $html = '<input type="text" id="webfavicon_url" name="theme_general_settings[webfavicon]" value="'.esc_url( $options['webfavicon'] ).'" />'; 
    $html .= '<input id="upload_favicon_button" type="button" class="button" value="Upload Favicon Logo" />';
    $html .= '<label for="webfavicon_url" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end webfavicon_callback

function webfavicon_preview_callback($args) {
     
    $options = get_option('theme_general_settings');

    if ($options['webfavicon']) {
	    $html = '<div id="upload_favicon_preview" style="max-height: 32px;">
	        <img style="max-width:100%;" src="'.esc_url( $options['webfavicon'] ).'" />
	    </div>';
    } else {
	    $html = '<div id="upload_favicon_preview" style="min-height: 100px;">Belum ada favicon logo.</div>';
    }
      
     
    echo $html;
     
} // end weblogo_callback 

function google_map_callback($args) {
     
    $options = get_option('theme_general_settings');

    $html = '<textarea cols="90" rows="5" id="google_map" name="theme_general_settings[google_map]">'. $options['google_map'] .'</textarea>';
    $html .= '<label for="google_map" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end google_map_callback

function google_map_preview_callback($args) {
     
    $options = get_option('theme_general_settings');

    $html = '<div id="google_map_preview" name="theme_general_settings[google_map_preview]">'. $options['google_map'] .'</div>';
     
    echo $html;
     
} // end google_map_preview_callback

function meta_slider_callback($args) {
     
    $options = get_option('theme_general_settings');

    $html = '<textarea cols="40" rows="4" id="meta_slider" name="theme_general_settings[meta_slider]">'. $options['meta_slider'] .'</textarea>';
    $html .= '<label for="meta_slider" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end meta_slider_callback

function contact_form_callback($args) {
     
    $options = get_option('theme_general_settings');

    $html = '<textarea cols="40" rows="4" id="contact_form" name="theme_general_settings[contact_form]">'. $options['contact_form'] .'</textarea>';
    $html .= '<label for="contact_form" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end contact_form_callback

function social_facebook_callback($args) {
     
    $options = get_option('theme_social_options');
     
    $html = '<span>http://www.facebook.com/</span><input type="text" id="social_facebook" name="theme_social_options[social_facebook]" size="50" value="' . $options['social_facebook']. '" />'; 
    $html .= '<label for="social_facebook" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end seobar_text_callback

function social_twitter_callback($args) {
     
    $options = get_option('theme_social_options');
     
    $html = '<span>http://www.twitter.com/</span><input type="text" id="social_twitter" name="theme_social_options[social_twitter]" size="50" value="' . $options['social_twitter']. '" />'; 
    $html .= '<label for="social_twitter" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end seobar_text_callback

function social_googleplus_callback($args) {
     
    $options = get_option('theme_social_options');
     
    $html = '<span>http://plus.google.com/</span><input type="text" id="social_googleplus" name="theme_social_options[social_googleplus]" size="50" value="' . $options['social_googleplus']. '" />'; 
    $html .= '<label for="social_googleplus" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end seobar_text_callback

function meta_keywords_callback($args) {
     
    $options = get_option('theme_seo_options');

    $html = '<textarea cols="90" rows="5" id="meta_keywords" name="theme_seo_options[meta_keywords]">'. $options['meta_keywords'] .'</textarea>';
    $html .= '<label for="meta_keywords" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end meta_keywords_callback

function meta_desc_callback($args) {
     
    $options = get_option('theme_seo_options');

    $html = '<textarea cols="90" rows="5" id="meta_desc" name="theme_seo_options[meta_desc]">'. $options['meta_desc'] .'</textarea>';
    $html .= '<label for="meta_desc" style="display: block; margin-top: 3px;"> '  . $args[0] . '</label>'; 
     
    echo $html;
     
} // end meta_desc_callback

/* ------------------------------------------------------------------------ *
 * Additional Functions
 * ------------------------------------------------------------------------ */

